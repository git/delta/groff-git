'\" t
.TH @g@eqn @MAN1EXT@ "@MDATE@" "groff @VERSION@"
.SH Name
@g@eqn \- format equations for
.I groff
or MathML
.
.
.\" ====================================================================
.\" Legal Terms
.\" ====================================================================
.\"
.\" Copyright (C) 1989-2023 Free Software Foundation, Inc.
.\"
.\" Permission is granted to make and distribute verbatim copies of this
.\" manual provided the copyright notice and this permission notice are
.\" preserved on all copies.
.\"
.\" Permission is granted to copy and distribute modified versions of
.\" this manual under the conditions for verbatim copying, provided that
.\" the entire resulting derived work is distributed under the terms of
.\" a permission notice identical to this one.
.\"
.\" Permission is granted to copy and distribute translations of this
.\" manual into another language, under the above conditions for
.\" modified versions, except that this permission notice may be
.\" included in translations approved by the Free Software Foundation
.\" instead of in the original English.
.
.
.\" Save and disable compatibility mode (for, e.g., Solaris 10/11).
.do nr *groff_eqn_1_man_C \n[.cp]
.cp 0
.
.\" Define fallback for groff 1.23's MR macro if the system lacks it.
.nr do-fallback 0
.if !\n(.f           .nr do-fallback 1 \" mandoc
.if  \n(.g .if !d MR .nr do-fallback 1 \" older groff
.if !\n(.g           .nr do-fallback 1 \" non-groff *roff
.if \n[do-fallback]  \{\
.  de MR
.    ie \\n(.$=1 \
.      I \%\\$1
.    el \
.      IR \%\\$1 (\\$2)\\$3
.  .
.\}
.rr do-fallback
.
.
.ie \n(.V<\n(.v \
.  ds tx T\h'-.1667m'\v'.224m'E\v'-.224m'\h'-.125m'X
.el \
.  ds tx TeX
.
.
.\" ====================================================================
.SH Synopsis
.\" ====================================================================
.
.SY @g@eqn
.RB [ \-CNrR ]
.RB [ \- d
.IR xy ]
.RB [ \-f
.IR F ]
.RB [ \-m
.IR n ]
.RB [ \-M
.IR dir ]
.RB [ \-p
.IR n ]
.RB [ \-s
.IR n ]
.RB [ \-T
.IR name ]
.RI [ file\~ .\|.\|.]
.YS
.
.
.SY @g@eqn
.B \-\-help
.YS
.
.
.SY @g@eqn
.B \-v
.
.SY @g@eqn
.B \-\-version
.YS
.
.
.\" ====================================================================
.SH Description
.\" ====================================================================
.
The GNU implementation of
.I eqn \" GNU
is part of the
.MR groff @MAN7EXT@
document formatting system.
.
.I @g@eqn
is a
.MR @g@troff @MAN1EXT@
preprocessor that translates descriptions of equations embedded in
.MR roff @MAN7EXT@
input files into the language understood by
.MR @g@troff @MAN1EXT@ .
.
It copies the contents of each
.I file
to the standard output stream,
except that lines between
.B .EQ
and
.B .EN
(or \[lq]inline\[rq] within a pair of user-specified delimiters)
are interpreted as equation descriptions.
.
Normally,
.I @g@eqn
is not executed directly by the user,
but invoked by specifying the
.B \-e
option to
.MR groff @MAN1EXT@ .
.
While GNU
.IR eqn 's \" GNU
input syntax is highly compatible with AT&T
.IR eqn , \" AT&T
the output
.I @g@eqn
produces cannot be processed by AT&T
.IR troff ; \" AT&T
GNU
.I troff \" GNU
(or a
.I troff \" generic
implementing relevant GNU extensions)
must be used.
.
If no
.I file
operands are given on the command line,
or if
.I file
is
.RB \[lq] \- \[rq],
the standard input stream is read.
.
.
.LP
Unless the
.B \-R
option is given,
.I @g@eqn
searches for the file
.I eqnrc
in the directories given with the
.B \-M
option first,
then in
.if !'@COMPATIBILITY_WRAPPERS@'no' .IR @SYSTEMMACRODIR@ ,
.IR @LOCALMACRODIR@ ,
and finally in the standard macro directory
.IR @MACRODIR@ .
.
If it exists and is readable,
.I @g@eqn
processes it before any input files.
.
.
.P
This man page primarily discusses the differences between GNU
.I eqn \" GNU
and AT&T
.IR eqn .\" AT&T
.
Most of the new features of the GNU
.I eqn \" GNU
input language are based on \*[tx].
.
There are some references to the differences between \*[tx] and GNU
.I eqn \" GNU
below;
these may safely be ignored if you do not know \*[tx].
.
.
.P
Three points are worth special note. \" good, bad, and different
.
.
.IP \[bu] 2n
GNU
.I eqn \" GNU
emits Presentation MathML output when invoked with the
.RB \[lq] "\-T\~MathML" \[rq]
option.
.
.
.IP \[bu]
GNU
.I eqn \" GNU
does not provide the functionality of
.IR neqn : \" AT&T
it does not support low-resolution,
typewriter-like devices
(although it may work adequately for very simple input).
.
.
.IP \[bu]
GNU
.I eqn
sets the input token
.RB \[lq] .\|.\|.\& \[rq]
as three periods or low dots,
rather than the three centered dots of
AT&T
.IR eqn . \" AT&T
.
To get three centered dots,
write
.B cdots
or
.RB \[lq] "cdot cdot cdot" \[rq].
.
.
.\" ====================================================================
.SS "Automatic spacing"
.\" ====================================================================
.
.I eqn
imputes a type to each component of an equation,
adjusting the spacing between them accordingly.
.
Recognized types are as follows.
.
.
.IP
.TS
lf(CR) l.
ordinary	T{
an ordinary character such as \[lq]1\[rq] or
.RI \[lq] x \[rq]
T}
operator	T{
a large operator such as
.ds Su \[lq]\s+5\[*S]\s0\[rq]
.if \n(.g .if !c\[*S] .ds Su the summation operator
\*[Su]
.rm Su
T}
binary	a binary operator such as \[lq]\[pl]\[rq]
relation	a relation such as \[lq]=\[rq]
opening	an opening bracket such as \[lq](\[rq]
closing	a closing bracket such as \[lq])\[rq]
punctuation	a punctuation character such as \[lq],\[rq]
inner	a sub-formula contained within brackets
suppress	a type without automatic spacing adjustment
.TE
.
.
.P
Two primitives apply types to equation components.
.
Quote type names in
.I eqn
commands to prevent macro expansion from being attempted on their names.
.
.
.TP
.BI type\~ "t e"
Apply
.RI type\~ t
to
.RI expression\~ e .
.
For example,
.RB \[lq] times \[rq]
is defined as if by the following.
.
.
.RS
.IP
.EX
define times \[aq] type "binary" \[rs](mu \[aq]
.EE
.RE
.
.
.br
.ne 7v
.TP
.BI chartype\~ "t text"
Each (unquoted) character
.RI in\~ text
is assigned
.RI type\~ t ,
persistently.
.
If
.I t
is
.RB \[lq] letter \[rq]
or
.RB \[lq] digit \[rq],
.B \%chartype
also assigns a typeface to each character in
.IR text .
.
See subsection \[lq]Fonts\[rq] below.
.
As an example,
the default spacing for some punctuation characters is set up as if
by the following
.I eqn \" generic
command.
.
.
.RS
.IP
.EX
chartype "punctuation" .,;:
.EE
.RE
.
.
.\" ====================================================================
.SS Primitives
.\" ====================================================================
.
.I @g@eqn
supports without alteration the AT&T
.I eqn \" AT&T
primitives
.BR above ,
.BR back ,
.BR bar ,
.BR bold ,
.BR \%define ,
.BR down ,
.BR fat ,
.BR font ,
.BR from ,
.BR fwd ,
.BR gfont ,
.BR gsize ,
.BR italic ,
.BR left ,
.BR lineup ,
.BR mark ,
.BR \%matrix ,
.BR \%ndefine ,
.BR over ,
.BR right ,
.BR roman ,
.BR size ,
.BR sqrt ,
.BR sub ,
.BR sup ,
.BR \%tdefine ,
.BR to ,
.BR \%under ,
and
.BR up .
.
.
.\" ====================================================================
.SS "New primitives"
.\" ====================================================================
.
The GNU extension primitives
.RB \[lq] type \[rq]
and
.B \%chartype
are discussed in subsection \[lq]Automatic spacing\[rq] above;
.RB \[lq] set \[rq]
in subsection \[lq]Customization\[rq] below;
and
.B grfont
and
.B gbfont
in subsection \[lq]Fonts\[rq] below.
.
.
.TP
.BI big\~ e
Enlarges the expression it modifies;
intended to have semantics like
CSS \[lq]large\[rq].
.
In
.I @g@troff
output,
the type size is increased by\~5.
.
MathML output emits the following.
.
.
.RS
.IP
.EX
<mstyle \%mathsize=\[aq]big\[aq]>
.EE
.RE
.
.
.TP
.IB e1 \~smallover\~ e2
This is similar to
.BR over ;
.B smallover
reduces the size of
.I e1
and
.IR e2 ;
it also puts less vertical space between
.I e1
or
.I e2
and the fraction bar.
.
The
.B over
primitive corresponds to the \*[tx]
.B \[rs]over
primitive in display styles;
.B smallover
corresponds to
.B \[rs]over
in non-display styles.
.
.
.TP
.BI vcenter\~ e
This vertically centers
.I e
about the math axis.
.
The math axis is the vertical position about which characters such as
\[lq]\[pl]\[rq] and \[lq]\[mi]\[rq] are centered;
it is also the vertical position used for fraction bars.
.
For example,
.B sum
is defined as follows.
.
.RS
.IP
.EX
{ type "operator" vcenter size +5 \[rs](*S }
.EE
.RE
.
.IP
.B vcenter
is silently ignored when generating MathML.
.
.
.TP
.IB e1 \~accent\~ e2
This sets
.I e2
as an accent over
.IR e1 .
.I e2
is assumed to be at the correct height for a lowercase letter;
.I e2
is moved down according to whether
.I e1
is taller or shorter than a lowercase letter.
.
For example,
.B hat
is defined as follows.
.
.
.RS
.IP
.EX
accent { "\[ha]" }
.EE
.RE
.
.
.IP
.BR dotdot ,
.BR dot ,
.BR tilde ,
.BR vec ,
and
.B dyad
are also defined using the
.B \%accent
primitive.
.
.
.TP
.IB e1 \~uaccent\~ e2
This sets
.I e2
as an accent under
.IR e1 .
.I e2
is assumed to be at the correct height for a character without a
descender;
.I e2
is moved down if
.I e1
has a descender.
.
.B utilde
is predefined using
.B uaccent
as a tilde accent below the baseline.
.
.
.TP
.BI "split \[dq]" text \[dq]
This has the same effect as simply
.
.
.RS
.IP
.EX
.I text
.EE
.RE
.
.
.IP
but
.I text
is not subject to macro expansion because it is quoted;
.I text
is split up and the spacing between individual characters is adjusted.
.
.
.TP
.BI nosplit\~ text
This has the same effect as
.
.
.RS
.IP
.EX
.RI \[dq] text \[dq]
.EE
.RE
.
.
.IP
but because
.I text
is not quoted it is subject to macro expansion;
.I text
is not split up and the spacing between individual characters is not
adjusted.
.
.
.TP
.IB e\~ opprime
This is a variant of
.B prime
that acts as an operator
.RI on\~ e .
.
It produces a different result from
.B prime
in a case such as
.RB \[lq] "A opprime sub 1" \[rq]:
with
.B \%opprime
the\~\[lq]1\[rq] is tucked under the prime as a subscript to
the\~\[lq]A\[rq]
(as is conventional in mathematical typesetting),
whereas with
.B prime
the\~\[lq]1\[rq] is a subscript to the prime character.
.
The precedence of
.B \%opprime
is the same as that of
.B bar
and
.BR \%under ,
which is higher than that of everything except
.B \%accent
and
.BR uaccent .
.
In unquoted text,
a neutral apostrophe
.RB ( \[aq] )
that is not the first character on the input line is treated like
.BR \%opprime .
.
.
.TP
.BI special\~ "text e"
Construct an object by calling the
.I troff \" generic
macro
.I text
.RI on\~ e .
.
The
.I troff \" generic
string
.B 0s
contains the
.I eqn \" generic
output
.RI for\~ e ,
and the registers
.BR 0w ,
.BR 0h ,
.BR 0d ,
.BR 0skern ,
and
.B 0skew
the width,
height,
depth,
subscript kern,
and skew
.RI of\~ e ,
respectively.
.
(The
.I subscript kern
of an object indicates how much a subscript on that object should be
\[lq]tucked in\[rq],
or placed to the left relative to a non-subscripted glyph of the same
size.
.
The
.I skew
of an object is how far to the right of the center of the object an
accent over it should be placed.)
.
The macro must modify
.B 0s
so that it outputs the desired result,
returns the drawing position to the text baseline at the beginning of
.IR e ,
and updates the foregoing registers to correspond to the new dimensions
of the result.
.
.
.IP
For example,
suppose you wanted a construct that \[lq]cancels\[rq] an expression by
drawing a diagonal line through it.
.
.
.br
.ne 11v
.RS
.IP
.EX
\&.de Ca
\&.  ds 0s \[rs]
\[rs]Z\[aq]\[rs]\[rs]*(0s\[aq]\[rs]
\[rs]v\[aq]\[rs]\[rs]n(0du\[aq]\[rs]
\[rs]D\[aq]l \[rs]\[rs]n(0wu \-\[rs]\[rs]n(0hu\-\[rs]\
\[rs]n(0du\[aq]\[rs]
\[rs]v\[aq]\[rs]\[rs]n(0hu\[aq]
\&..
\&.EQ
special Ca "x \[rs][mi] 3 \[rs][pl] x" \[ti] 3
\&.EN
.EE
.RE
.
.
.IP
We use the
.B \[rs][mi]
and
.B \[rs][pl]
special characters instead of + and \-
because they are part of the argument to a
.I @g@troff
macro,
so
.I @g@eqn
does not transform them to mathematical glyphs for us.
.
Here's a more complicated construct that draws a box around an
expression;
the bottom of the box rests on the text baseline.
.
We define the
.I eqn \" generic
macro
.B box
to wrap the call of the
.I @g@troff
macro
.BR Bx .
.
.
.br
.ne 17v
.RS
.IP
.EX
\&.de Bx
\&.ds 0s \[rs]
\[rs]Z\[aq]\[rs]\[rs]h\[aq]1n\[aq]\[rs]\[rs]*[0s]\[aq]\[rs]
\[rs]v\[aq]\[rs]\[rs]n(0du+1n\[aq]\[rs]
\[rs]D\[aq]l \[rs]\[rs]n(0wu+2n 0\[aq]\[rs]
\[rs]D\[aq]l 0 \-\[rs]\[rs]n(0hu\-\[rs]\[rs]n(0du\-2n\[aq]\[rs]
\[rs]D\[aq]l \-\[rs]\[rs]n(0wu\-2n 0\[aq]\[rs]
\[rs]D\[aq]l 0 \[rs]\[rs]n(0hu+\[rs]\[rs]n(0du+2n\[aq]\[rs]
\[rs]h\[aq]\[rs]\[rs]n(0wu+2n\[aq]
\&.nr 0w +2n
\&.nr 0d +1n
\&.nr 0h +1n
\&..
\&.EQ
define box \[aq] special Bx $1 \[aq]
box(foo) \[ti] "bar"
\&.EN
.EE
.RE
.
.
.br
.ne 5v
.TP
.BI space\~ n
Set extra vertical spacing around the equation,
replacing the default values,
where
.IR n \~is
an integer in hundredths of an em.
.
If positive,
.IR n \~increases
vertical spacing before the equation;
if negative,
it does so after the equation.
.
This primitive provides an interface to
.IR groff 's
.B \[rs]x
escape sequence,
but with the opposite sign convention.
.
It has no effect if the equation is part of a
.MR @g@pic @MAN1EXT@
picture.
.
.
.\" ====================================================================
.SS "Extended primitives"
.\" ====================================================================
.
.I @g@eqn
recognizes an
.RB \[lq] on \[rq]
argument to the
.B delim
primitive specially,
restoring any delimiters previously disabled with
.RB \[lq] "delim off" \[rq].
.
If delimiters haven't been specified,
neither command has effect.
.
.
.TP
.BI col\~ n\~\c
.BR {\~ .\|.\|.\& \~}
.TQ
.BI ccol\~ n\~\c
.BR {\~ .\|.\|.\& \~}
.TQ
.BI lcol\~ n\~\c
.BR {\~ .\|.\|.\& \~}
.TQ
.BI rcol\~ n\~\c
.BR {\~ .\|.\|.\& \~}
.TQ
.BI pile\~ n\~\c
.BR {\~ .\|.\|.\& \~}
.TQ
.BI cpile\~ n\~\c
.BR {\~ .\|.\|.\& \~}
.TQ
.BI lpile\~ n\~\c
.BR {\~ .\|.\|.\& \~}
.TQ
.BI rpile\~ n\~\c
.BR {\~ .\|.\|.\& \~}
The integer
.RI value\~ n
(in hundredths of an em)
increases the vertical spacing between rows,
using
.IR groff 's
.B \[rs]x
escape sequence
(the value has no effect in MathML mode).
.
Negative values are possible but have no effect.
.
If more than one
.I n
occurs in a matrix,
the largest is used.
.
.
.\" ====================================================================
.SS Customization
.\" ====================================================================
.
When
.I eqn
generates
.I troff \" generic
input,
the appearance of equations is controlled by a large number of
parameters.
.
They have no effect when generating MathML mode,
which pushes typesetting and fine motions downstream to a MathML
rendering engine.
.
These parameters can be set using the
.B set
primitive.
.
.
.TP
.BI set\~ "p n"
This sets
.RI parameter\~ p
to
.RI value\~ n ,
where
.IR n \~is
an integer.
.
For example,
.
.
.RS
.IP
.EX
set x_height 45
.EE
.RE
.
.
.IP
says that
.I @g@eqn
should assume an x\~height of 0.45\~ems.
.
.
.RS
.LP
Possible parameters are as follows.
.
Values are in units of hundredths of an em unless otherwise stated.
.
These descriptions are intended to be expository rather than
definitive.
.
.
.TP
.B minimum_size
.I @g@eqn
won't set anything at a smaller type size than this.
.
The value is in points.
.
.
.TP
.B fat_offset
The
.B fat
primitive emboldens an equation by overprinting two copies of the
equation horizontally offset by this amount.
.
This parameter is not used in MathML mode;
fat text uses
.
.RS
.RS
.EX
<mstyle mathvariant=\[aq]double\-struck\[aq]>
.EE
.RE
.
instead.
.RE
.
.
.TP
.B over_hang
A fraction bar is longer by twice this amount than
the maximum of the widths of the numerator and denominator;
in other words,
it overhangs the numerator and denominator by at least this amount.
.
.
.TP
.B accent_width
When
.B bar
or
.B \%under
is applied to a single character,
the line is this long.
.
Normally,
.B bar
or
.B \%under
produces a line whose length is the width of the object to which it
applies;
in the case of a single character,
this tends to produce a line that looks too long.
.
.
.TP
.B delimiter_factor
Extensible delimiters produced with the
.B left
and
.B right
primitives have a combined height and depth of at least this many
thousandths of twice the maximum amount by which the sub-equation that
the delimiters enclose extends away from the axis.
.
.
.TP
.B delimiter_shortfall
Extensible delimiters produced with the
.B left
and
.B right
primitives have a combined height and depth not less than the
difference of twice the maximum amount by which the sub-equation that
the delimiters enclose extends away from the axis and this amount.
.
.
.TP
.B null_delimiter_space
This much horizontal space is inserted on each side of a fraction.
.
.
.TP
.B script_space
The width of subscripts and superscripts is increased by this amount.
.
.
.TP
.B thin_space
This amount of space is automatically inserted after punctuation
characters.
.
.
.TP
.B medium_space
This amount of space is automatically inserted on either side of
binary operators.
.
.
.TP
.B thick_space
This amount of space is automatically inserted on either side of
relations.
.
.
.TP
.B x_height
The height of lowercase letters without ascenders such as \[lq]x\[rq].
.
.
.TP
.B axis_height
The height above the baseline of the center of characters such as
\[lq]\[pl]\[rq] and \[lq]\[mi]\[rq].
.
It is important that this value is correct for the font
you are using.
.
.
.TP
.B default_rule_thickness
This should be set to the thickness of the
.B \[rs][ru]
character,
or the thickness of horizontal lines produced with the
.B \[rs]D
escape sequence.
.
.
.TP
.B num1
The
.B over
primitive shifts up the numerator by at least this amount.
.
.
.TP
.B num2
The
.B smallover
primitive shifts up the numerator by at least this amount.
.
.
.TP
.B denom1
The
.B over
primitive shifts down the denominator by at least this amount.
.
.
.TP
.B denom2
The
.B smallover
primitive shifts down the denominator by at least this amount.
.
.
.TP
.B sup1
Normally superscripts are shifted up by at least this amount.
.
.
.TP
.B sup2
Superscripts within superscripts or upper limits
or numerators of
.B smallover
fractions are shifted up by at least this amount.
.
This is usually less than
.BR sup1 .
.
.
.TP
.B sup3
Superscripts within denominators or square roots
or subscripts or lower limits are shifted up by at least
this amount.
.
This is usually less than
.BR sup2 .
.
.
.TP
.B sub1
Subscripts are normally shifted down by at least this amount.
.
.
.TP
.B sub2
When there is both a subscript and a superscript,
the subscript is shifted down by at least this amount.
.
.
.TP
.B sup_drop
The baseline of a superscript is no more than this much below the top of
the object on which the superscript is set.
.
.
.TP
.B sub_drop
The baseline of a subscript is at least this much below the bottom of
the object on which the subscript is set.
.
.
.TP
.B big_op_spacing1
The baseline of an upper limit is at least this much above the top of
the object on which the limit is set.
.
.
.TP
.B big_op_spacing2
The baseline of a lower limit is at least this much below the bottom
of the object on which the limit is set.
.
.
.TP
.B big_op_spacing3
The bottom of an upper limit is at least this much above the top of
the object on which the limit is set.
.
.
.TP
.B big_op_spacing4
The top of a lower limit is at least this much below the bottom of the
object on which the limit is set.
.
.
.TP
.B big_op_spacing5
This much vertical space is added above and below limits.
.
.
.TP
.B baseline_sep
The baselines of the rows in a pile or matrix are normally this far
apart.
.
In most cases this should be equal to the sum of
.B num1
and
.BR denom1 .
.
.
.TP
.B shift_down
The midpoint between the top baseline and the bottom baseline in a
matrix or pile is shifted down by this much from the axis.
.
In most cases this should be equal to
.BR axis_height .
.
.
.TP
.B column_sep
This much space is added between columns in a matrix.
.
.
.TP
.B matrix_side_sep
This much space is added at each side of a matrix.
.
.
.TP
.B draw_lines
If this is non-zero,
lines are drawn using the
.B \[rs]D
escape sequence,
rather than with the
.B \[rs]l
escape sequence and the
.B \[rs][ru]
character.
.
.
.TP
.B body_height
The amount by which the height of the equation exceeds this is added as
extra space before the line containing the equation
(using
.BR \[rs]x ).
.
The default value is 85.
.
.
.TP
.B body_depth
The amount by which the depth of the equation exceeds this is added as
extra space after the line containing the equation
(using
.BR \[rs]x ).
.
The default value is 35.
.
.
.TP
.B nroff
If this is non-zero,
then
.B \%ndefine
behaves like
.B \%define
and
.B \%tdefine
is ignored,
otherwise
.B \%tdefine
behaves like
.B \%define
and
.B \%ndefine
is ignored.
.
The default value is\~0;
the
.I eqnrc
file sets it to\~1 for the
.BR ascii ,
.BR latin1 ,
.BR utf8 ,
and
.B cp1047
output devices.
.
.
.P
Appendix\~H
of
.I "The \*[tx]book"
discusses many of these parameters in greater detail.
.RE
.
.
.\" ====================================================================
.SS Macros
.\" ====================================================================
.
In GNU
.IR eqn , \" GNU
macros can take arguments.
.
In a macro body,
.BI $ n\c
,
where
.I n
is between 1 and\~9,
is replaced by the
.IR n th
argument if the macro is called with arguments;
if there are fewer than
.IR n \~arguments,
it is replaced by nothing.
.
A word containing a left parenthesis where the part of the word before
the left parenthesis has been defined using the
.B \%define
primitive is recognized as a macro call with arguments;
characters following the left parenthesis up to a matching right
parenthesis are treated as comma-separated arguments.
.
Commas inside nested parentheses
do not terminate an argument.
.
In the following synopses,
.I X
can be any character not appearing in the parameter thus bracketed.
.
.
.TP
.BI sdefine\~ "name X anything X"
This is like the
.B \%define
primitive,
but
.I name
is not recognized if called with arguments.
.
.
.br
.ne 4v \" XXX: should need only 3v!
.TP
.BI include\~ file
.TQ
.BI copy\~ file
Interpolate the contents of
.IR file .
.
Lines in
.I file
beginning with
.B .EQ
or
.B .EN
are ignored.
.
.
.TP
.BI ifdef\~ "name X anything X"
If
.I name
has been defined by
.B \%define
(or has been automatically defined because
.I name
is the output driver)
process
.IR anything ;
otherwise ignore
.IR anything .
.
.
.TP
.BI undef\~ name
Remove definition of
.IR name ,
making it undefined.
.
.
.\" ====================================================================
.SS "Predefined macros"
.\" ====================================================================
.
GNU
.I eqn \" GNU
supports the predefined macros offered by AT&T
.IR eqn : \" AT&T
.BR and ,
.BR \%approx ,
.BR arc ,
.BR cos ,
.BR cosh ,
.BR del ,
.BR det ,
.BR dot ,
.BR \%dotdot ,
.BR dyad ,
.BR exp ,
.BR for ,
.BR grad ,
.BR half ,
.BR hat ,
.BR if ,
.BR \%inter ,
.BR Im ,
.BR inf ,
.BR int ,
.BR lim ,
.BR ln ,
.BR log ,
.BR max ,
.BR min ,
.BR \%nothing ,
.BR \%partial ,
.BR prime ,
.BR prod ,
.BR Re ,
.BR sin ,
.BR sinh ,
.BR sum ,
.BR tan ,
.BR tanh ,
.BR tilde ,
.BR times ,
.BR union ,
.BR vec ,
.BR == ,
.BR != ,
.BR += ,
.BR \-> ,
.BR <\- ,
.BR << ,
.BR >> ,
and
.RB \[lq] .\|.\|. \[rq].
.
The lowercase classical Greek letters are available as
.BR \%alpha ,
.BR beta ,
.BR chi ,
.BR delta ,
.BR \%epsilon ,
.BR eta ,
.BR gamma ,
.BR iota ,
.BR kappa ,
.BR lambda ,
.BR mu ,
.BR nu ,
.BR omega ,
.BR \%omicron ,
.BR phi ,
.BR pi ,
.BR psi ,
.BR rho ,
.BR sigma ,
.BR tau ,
.BR theta ,
.BR \%upsilon ,
.BR xi ,
and
.BR zeta .
.
Obtain their uppercase forms by spelling these names with an initial
capital letter or in full capitals,
as in
.B \%Alpha
or
.BR \%ALPHA .
.
.
.P
GNU
.I eqn \" GNU
further defines the macros
.BR cdot ,
.BR cdots ,
and
.B utilde
(all discussed above),
.BR \%dollar ,
which sets a dollar sign,
and
.BR ldots ,
which sets three dots on the baseline.
.
.
.\" ====================================================================
.SS Fonts
.\" ====================================================================
.
.I @g@eqn
uses up to three typefaces to set an equation:
italic (oblique),
roman (upright),
and bold.
.
Assign each a
.I groff
typeface with the primitives
.BR gfont ,
.BR \%grfont ,
and
.B \%gbfont.
.
The defaults are the styles
.BR I ,
.BR R ,
and
.B B
(applied to the current font family).
.
The
.B \%chartype
primitive
(see above)
sets a character's type,
which determines the face used to set it.
.
The \[lq]letter\[rq] type is set in italics,
and \[lq]digit\[rq] in roman.
.
Use the
.B bold
primitive to select an (upright) bold style.
.
.
.TP
.BI gbfont\~ f
.RI Select\~ f
as the bold font.
.
This is a GNU extension.
.
.
.TP
.BI gfont\~ f
.RI Select\~ f
as the italic font.
.
.
.TP
.BI grfont\~ f
.RI Select\~ f
as the roman font.
.
This is a GNU extension.
.
.
.\" ====================================================================
.SH Options
.\" ====================================================================
.
.B \-\-help
displays a usage message,
while
.B \-v
and
.B \-\-version
show version information;
all exit afterward.
.
.
.TP
.B \-C
Recognize
.B .EQ
and
.B .EN
even when followed by a character other than space or newline.
.
.
.TP
.BI \-d\~ xy
Specify delimiters
.I x
.RI and\~ y
for the left and right ends,
respectively,
of inline equations.
.
.I x
and
.I y
need not be distinct.
.
Any
.B delim
.I xy
statements in the source file override this option.
.
.
.TP
.BI \-f\~ F
is equivalent to
.RB \[lq] gfont
.IR F \[rq].
.
.
.TP
.BI \-m\~ n
Set the minimum type size to
.IR n \~points.
.
.I @g@eqn
will not reduce the size of sub- or superscripts beyond this size.
.
.
.TP
.BI \-M\~ dir
Search
.I dir
for
.I eqnrc
before those listed in section \[lq]Description\[rq] above.
.
.
.TP
.B \-N
Prohibit newlines within delimiters.
.
This option allows
.I @g@eqn
to recover better from missing closing delimiters.
.
.
.TP
.BI \-p\~ n
Set sub- and superscripts
.IR n \~points
smaller than the surrounding text.
.
This option is deprecated.
.
.I @g@eqn
normally sets sub- and superscripts at 70% of the type size of the
surrounding text.
.
.
.TP
.B \-r
Reduce the type size of subscripts at most once relative to the base
type size for the equation.
.
.
.TP
.B \-R
Don't load
.IR eqnrc .
.
.
.TP
.BI \-s\~ n
This is equivalent to a
.RB \[lq] gsize
.IR n \[rq]
command.
.
This option is deprecated.
.
.I @g@eqn
normally sets equations at the type size current when the equation is
encountered.
.
.
.TP
.BI \-T\~ name
Prepare output for the device
.IR name .
.
In most cases,
the effect of this is to define a macro
.I name
with a value
.RB of\~ 1 ;
.I eqnrc
uses this to provide definitions appropriate for the device.
.
However,
if the specified driver is \[lq]MathML\[rq],
the output is MathML markup rather than
.I @g@troff
input,
and
.I eqnrc
is not loaded at all.
.
The default output device is
.BR @DEVICE@ .
.
.
.\" ====================================================================
.SH Files
.\" ====================================================================
.
.TP
.I @MACRODIR@/\:\%eqnrc
Initialization file.
.
.
.\" ====================================================================
.SH "MathML mode limitations"
.\" ====================================================================
.
MathML is designed on the assumption that it cannot know the exact
physical characteristics of the media and devices on which it will
be rendered.
.
It does not support fine control of motions and sizes to the same
degree
.I @g@troff
does.
.
Thus:
.
.IP \[bu] 2n
.I @g@eqn
parameters have no effect on the generated MathML.
.
.IP \[bu]
The
.BR special ,
.BR up ,
.BR down ,
.BR fwd ,
and
.B back
operations cannot be implemented,
and yield a MathML \%\[lq]<merror>\[rq] message instead.
.
.IP \[bu]
The
.B vcenter
keyword is silently ignored,
as centering on the math axis is the MathML default.
.
.IP \[bu]
Characters that
.I @g@eqn
sets extra large in
.I troff \" mode
mode\[em]notably the integral sign\[em]may appear too small and need to
have their \[lq]<mstyle>\[rq] wrappers adjusted by hand.
.
.
.LP
As in its
.I troff \" mode
mode,
.I @g@eqn
in MathML mode leaves the
.B .EQ
and
.B .EN
delimiters in place for displayed equations,
but emits no explicit delimiters around inline equations.
.
They can,
however,
be recognized as strings that begin with \[lq]<math>\[rq] and end with
\[lq]</math>\[rq] and do not cross line boundaries.
.
.
.\" ====================================================================
.SH Bugs
.\" ====================================================================
.
Words must be quoted anywhere they occur in
.I eqn \" generic
input if they are not to be recognized as names of macros or primitives,
or if they are to be interpreted by
.IR troff . \" generic
.
These names,
particularly short ones like
.RB \[lq] pi \[rq]
and
.RB \[lq] PI \[rq],
can collide with
.I troff \" generic
identifiers.
.
For instance,
the
.I eqn \" generic
command
.
.RS
.EX
gfont PI
.EE
.RE
.
does not select
.IR groff 's
Palatino italic font for the \[lq]global\[rq] equation face;
you must use
.
.RS
.EX
gfont "PI"
.EE
.RE
.
instead.
.
.
.P
Inline equations are set at the type size that is current at the
beginning of the input line.
.
.
.LP
In MathML mode,
the
.B mark
and
.B lineup
features don't work.
.
These could,
in theory,
be implemented with \[lq]<maligngroup>\[rq] elements.
.
.
.LP
In MathML mode,
each digit of a numeric literal gets a separate \[lq]<mn>\:</mn>\[rq]
pair,
and decimal points are tagged with \[lq]<mo>\:</mo>\[rq].
.
This is allowed by the specification,
but inefficient.
.
.
.\" ====================================================================
.SH "See also"
.\" ====================================================================
.
\[lq]Typesetting Mathematics\[em]User's Guide\[rq]
(2nd edition),
by Brian W.\& Kernighan
and Lorinda L.\& Cherry,
1978,
AT&T Bell Laboratories Computing Science Technical Report No.\& 17.
.
.
.LP
.IR The\~\*[tx]book ,
by Donald E.\& Knuth,
1984,
Addison-Wesley Professional.
.
.
.LP
.MR groff_char @MAN7EXT@ ,
particularly subsections \[lq]Logical symbols\[rq],
\[lq]Mathematical symbols\[rq],
and \[lq]Greek glyphs\[rq],
documents a variety of special character escape sequences useful in
mathematical typesetting.
.
.
.LP
.MR groff @MAN1EXT@ ,
.MR @g@troff @MAN1EXT@ ,
.MR @g@pic @MAN1EXT@ ,
.MR groff_font @MAN5EXT@
.
.
.\" Clean up.
.rm tx
.
.\" Restore compatibility mode (for, e.g., Solaris 10/11).
.cp \n[*groff_eqn_1_man_C]
.do rr *groff_eqn_1_man_C
.
.
.\" Local Variables:
.\" fill-column: 72
.\" mode: nroff
.\" tab-width: 12
.\" End:
.\" vim: set filetype=groff tabstop=12 textwidth=72:
